#!/usr/bin/env python
"""
RO line level export - Not used
"""
import datetime
import os
import re
from time import time

import pandas as pd
from dateutil.relativedelta import relativedelta
from sqlalchemy import create_engine
from sqlalchemy import types as sqltypes
import mazdap as mz
from credentials import login

os.environ["NLS_LANG"] = ".AL32UTF8"

def rename_col(x):
    regex = re.compile(r"[\W]+")
    return regex.sub("_", x.lower())

def make_month_ids(months_back = 12):
    this_month = datetime.datetime.today().replace(day=1)
    months = [this_month - relativedelta(months=x) for x in range(months_back)]
    month_ids = [x.strftime('%Y%m') for  x in months]
    return month_ids

def get_active_dealer(uid=login["uid"], pwd=login["pwd"]):
   conn = mz.ObieeConnector(uid, pwd)
   query = """
   SELECT
      "Vehicle - Sales"."Dealer Hierarchy"."Dealer Cd" s_9,
   FROM "Vehicle - Sales"
   WHERE
   ("Retail Region"."Retail Country Cd" = 'US')
   AND ("Vehicle - Sales"."Dealer Detail"."Status Cd" = 'A')
   AND ("Vehicle - Sales"."Dealer Detail"."Mda Cd"  IS NOT NULL)
   """

   df = conn.exec_sql(query).rename(columns=rename_col)
   return df


def get_ro(month_id, uid=login["uid"], pwd=login["pwd"]):
    conn = mz.ObieeConnector(uid, pwd)
    query = """
    SELECT
       "Vehicle - Repair Order"."Attributes"."Repair Order Labor OPN Code" ,
       "Vehicle - Repair Order"."Attributes"."Service Pay Type" ,
       "Vehicle - Repair Order"."Date - Repair Order Completion"."RO Completion Calendar Date" ,
       "Vehicle - Repair Order"."Date - Repair Order Date"."Repair Order Calendar Date" ,
       "Vehicle - Repair Order"."Dealer - Repair Order"."RO Dealer Code" ,
       "Vehicle - Repair Order"."Repair Order"."RO Claim Code" ,
       "Vehicle - Repair Order"."Repair Order"."RO VIN Code" ,
       "Vehicle - Repair Order"."Vehicle - Master"."Retail Sale Date" ,
       "Vehicle - Repair Order"."Date - Repair Order Completion"."RO Completion Calendar Year Month ID"
    FROM "Vehicle - Repair Order"
    WHERE
    (
       ("Date - Repair Order Completion"."RO Completion Calendar Year Month ID" = '{month_id}') 
       AND ((("Attributes"."Service Pay Type" = 'C') OR ("Attributes"."Service Pay Type" = 'W')) OR ("Attributes"."Repair Order Labor OPN Code" = '55R'))
       AND ( "Vehicle - Repair Order"."Dealer - Repair Order"."RO Dealer Code" = '{dlr_cd}')
       )
    """

    colnames = [
        "lbr_op_code",
        "pay_type",
        "ro_complete_dt",
        "ro_dt",
        "dlr_cd",
        "ro_claim_cd",
        "vin",
        "rdr_dt",
        "monthid",
    ]

    dealers = get_active_dealer()

    dfs = []
    for dlr in dealers["dealer_code"]:
        q = query.format(dlr_cd=dlr, month_id=month_id)
        df = conn.exec_sql(q).drop_duplicates()
        dfs.append(df)

    try:
        df = pd.concat(dfs, axis=0, ignore_index=True)
        df.columns = colnames
        dt_cols = df.columns[df.columns.str.endswith("_dt")]
        df[dt_cols] = df[dt_cols].apply(pd.to_datetime, errors="coerce")

        df["ro_rdr_dt_diff"] = (df["ro_complete_dt"] - df["rdr_dt"]).dt.days

        return df
    except Exception as e:
        logger.error(e)
        pass


engine = create_engine(
    "oracle+cx_oracle://RPR_STG:rpSgdEv12@x4ebid10.mnao.net:1521/EDWHDEV",
    max_identifier_length=128,
)

for id in  make_month_ids(12):
    s = time()
    ro_df = get_ro(id)
    print("Month: {} took {} seconds".format(id, time()-s))

    if ro_df is not None:
        sql_dtype = {
            x: (sqltypes.DATE if x.endswith("_dt") else sqltypes.VARCHAR(50))
            for x in ro_df.columns
        }

        sql_dtype.update({"ro_rdr_dt_diff": sqltypes.NUMERIC})
        ro_df.to_sql("fw_ro_export", engine, if_exists="append", index=False, dtype=sql_dtype)






#%%


#%%

# ro_df["RO_CAL_DATE"] = pd.to_datetime(ro_df["RO_CAL_DATE"])
# ro_df["RTL_SLS_DATE"] = pd.to_datetime(ro_df["RTL_SLS_DATE"], errors="coerce")
# ro_df[["RO_TOT_AMT", "RO_PART_TOT_AMT"]] = ro_df[
#     ["RO_TOT_AMT", "RO_PART_TOT_AMT"]
# ].astype(float)
# ro_df["RO_NUMBER"] = ro_df["RO_NUMBER"].str.lstrip("0")

# with open("ro_data.pkl", "wb") as output:  # Overwrites any existing file.
#     pickle.dump(ro_df, output, pickle.HIGHEST_PROTOCOL)

# os.environ["NLS_LANG"] = ".AL32UTF8"

# engine = create_engine(
#     "oracle+cx_oracle://RPR_STG:rpSgdEv12@x4ebid10.mnao.net:1521/EDWHDEV"
# )

# dtypes = {}
# str_cols = [
#     "DEALER_CODE",
#     "VIN",
#     "DLR_NAME",
#     "RO_NUMBER",
#     "RO_CLAIM_SEQ_NUMBER",
#     "SERVICE_PAY_TYPE",
# ]

# for i in ro_df.columns:
#     if i in ["RO_CAL_DATE", "RTL_SLS_DATE"]:
#         dtypes[i] = DateTime()
#     elif i in str_cols:
#         dtypes[i] = String(500)
#     else:
#         dtypes[i] = Float()


# # ro_df.to_sql(
# #     "fw_dst_ro_records", engine, if_exists="replace", index=False, dtype=dtypes
# # )
