import pandas as pd

from util import create_view, get_db_query


def get_dealer_enrollment() -> pd.DataFrame:
    """
    Mirror of VIEW_FW_DS_DEALER_ENROLLMENT
    """    
    path = "../sql/view_fw_ds_dealer_enrollment.sql"
    create_view(path)

    query = """SELECT * FROM rpr_stg.view_fw_ds_dealer_enrollment"""
    df =  get_db_query(query) 
    df.to_csv("../data/fact_dealer_enrollment.csv", index=False)

    return df
