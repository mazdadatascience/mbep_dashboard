#%%
from module_logger import logger
from util import get_db_query, rclone_copy


def get_fact_digital_service_updt():
    with open("../sql/view_fw_latest_ro_response.sql") as f:
        query = f.read()

    get_db_query(query).to_csv(
        "../data/fact_digital_service_updt_hist.csv", index=False
    )
    logger.info(f"getting digital service providers update history - OK")
