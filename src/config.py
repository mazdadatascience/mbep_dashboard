ENV="PROD"
FEATURE=None
SHAREPOINT_COPY=True
REPORT_MONTHS=9

LOG_LEVEL='DEBUG'


assert ENV in ("DEV", "PROD")

if ENV=='PROD':
    PATH_SUFFIX=None
elif ENV=='DEV':
    PATH_SUFFIX=f"DEV_{FEATURE}" if FEATURE else "DEV"

if PATH_SUFFIX:
    LOG_PATH=f"../mbep_{PATH_SUFFIX}.log".lower()
else:
    LOG_PATH=f"../mbep.log".lower()

