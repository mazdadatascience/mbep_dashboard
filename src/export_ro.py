#!/usr/bin/env python
"""This script uses mazdap to gather all Customer Pay and Warranty ROs
from the 'Vehicle - Repair Order Claim' subject area and writes to Oracle DB.

Time Period: Previous 12 months based on current date
Table(s): FW_RO_EXPORT
"""
import os
import re
from datetime import datetime as dt
from time import time

import pandas as pd
from sqlalchemy import types as sqltypes

from config import PATH_SUFFIX
from module_logger import logger
from util import ENGINE, OBCONN, get_report_months

os.environ["NLS_LANG"] = ".AL32UTF8"


def export_obiee_ro_to_db(start_month):
    """export OBIEE RO headers to staging table"""
    if PATH_SUFFIX:
        table = "_".join(["fw_ro_export", PATH_SUFFIX]).lower()
    else:
        table = "fw_ro_export"

    logger.info(f"Staging: OBIEE ROs to {table}")
    try:
        ENGINE.execute("drop table {}".format(table))
    except:
        pass

    dlr_state = get_dlr_state()

    for month in get_report_months(start_month):
        s = time()
        ro_df = get_ro(month)
        ro_df = ro_df.merge(dlr_state, on="dlr_cd", how="inner")

        if ro_df is not None:
            ro_df["updt_dt"] = dt.today()

            dtypes = {x: sqltypes.VARCHAR(50) for x in ro_df.columns}
            dtypes.update(
                {
                    x: sqltypes.FLOAT
                    for x in ro_df.columns
                    if re.search("_amt|dt_diff", x)
                }
            )
            dtypes.update(
                {x: sqltypes.DATE for x in ro_df.columns if x.endswith("_dt")}
            )

            ro_df.to_sql(table, ENGINE, if_exists="append", index=False, dtype=dtypes)
            logger.debug(
                "Month: {} took {:.1f} seconds. {}rows".format(
                    month, time() - s, ro_df.shape[0]
                )
            )
    logger.info("Finished staging OBIEE ROs")


def get_ro(month_id):

    query = """
    SELECT
       "Vehicle - Repair Order Claim"."Date - Repair Order Completion"."RO Completion Calendar Date" s_2,
       "Vehicle - Repair Order Claim"."Date - Repair Order Completion"."RO Completion Calendar Year Month ID" s_3,
       "Vehicle - Repair Order Claim"."Dealer Attributes"."Dealer Inactive Date" s_4,
       "Vehicle - Repair Order Claim"."Dealer"."Dealer Code" s_5,
       "Vehicle - Repair Order Claim"."Dealer"."Status Code" s_6,
       "Vehicle - Repair Order Claim"."Facts"."RO Claim Code" s_7,
       "Vehicle - Repair Order Claim"."Facts"."RO VIN Code" s_8,
       "Vehicle - Repair Order Claim"."Vehicle - Master"."Retail Sale Date" s_10,
       "Vehicle - Repair Order Claim"."Facts"."Cust Total Amt" s_11,
       "Vehicle - Repair Order Claim"."Facts"."Internal Amt" s_12,
       "Vehicle - Repair Order Claim"."Facts"."Warr Total Amt" s_13,
       "Vehicle - Repair Order Claim"."Facts"."Total Cust Part Amt" s_14
    FROM "Vehicle - Repair Order Claim"
    WHERE
    (
        ("Dealer Hierarchy"."RO Country Code" = 'US') 
        AND ("Dealer"."Status Code" = 'A') 
        AND (
            ("Dealer Attributes"."Dealer Inactive Date" <= timestamp '1900-01-01 00:00:00') 
            OR ("Dealer Attributes"."Dealer Inactive Date" IS NULL) 
        ) 
        AND ("Date - Repair Order Completion"."RO Completion Calendar Year Month ID" = {month_id}) 
        AND (
            ("Facts"."Cust Total Amt" > 0) 
            OR (
                ("Facts"."Warr Total Amt" > 0) 
                AND (("Facts"."Internal Amt" <= 0) OR ("Facts"."Internal Amt" IS NULL))
            )
        )
    )
    """

    colname = [
        "rpr_complete_dt",
        "rpr_month_id",
        "dlr_inactive_dt",
        "dlr_cd",
        "dlr_status_code",
        "ro_claim_cd",
        "vin",
        "rtl_sale_dt",
        "cust_total_amt",
        "internal_amt",
        "warr_total_amt",
        "cust_part_amt",
    ]

    amt_cols = [x for x in colname if x.endswith("_amt")]
    dt_columns = [x for x in colname if x.endswith("_dt")]

    try:
        q = query.format(month_id=month_id)
        df = OBCONN.exec_sql(q).drop_duplicates()
        df.columns = colname

        df[dt_columns] = df[dt_columns].apply(
            lambda x: pd.to_datetime(x, errors="coerce")
        )
        df["ro_sale_dt_diff"] = (df["rpr_complete_dt"] - df["rtl_sale_dt"]).dt.days
        df[amt_cols] = df[amt_cols].astype("float")
        df["pay_type"] = ["C" if x > 0 else "W" for x in df["cust_total_amt"]]

        return df
    except Exception as e:
        logger.warning(f"OBIEE {month_id} RO's has {df.shape[0]} rows")
        breakpoint()
        logger.exception(e)
        return None


def get_dlr_state():
    """Getting this in a separate query to because records would be dropped otherwise???"""
    q = """
    SELECT
    "Vehicle - Repair Order Claim"."Dealer"."Dealer Code" s_1,
    "Vehicle - Repair Order Claim"."Location"."State Code" s_2
    FROM "Vehicle - Repair Order Claim"
    WHERE
    (("Dealer"."Status Code" = 'A') AND ("Dealer"."Country Code" = 'US'))
    """

    columns = ["dlr_cd", "state"]

    try:
        df = OBCONN.exec_sql(q)
        df.columns = columns
        return df
    except Exception as e:
        logger.exception(e)
