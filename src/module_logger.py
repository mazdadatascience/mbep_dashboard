from mazdap import create_logger

from config import LOG_LEVEL, LOG_PATH

logger = create_logger(LOG_PATH, LOG_LEVEL)
