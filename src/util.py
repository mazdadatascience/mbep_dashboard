from datetime import datetime
from dateutil.relativedelta import relativedelta
import os
import re
import pandas as pd
from sqlalchemy import create_engine

from config import ENV, FEATURE, SHAREPOINT_COPY
from module_logger import logger
from mazdap import ObieeConnector
from credentials import login, EDWPWD

ENGINE = create_engine(f"oracle://rpr_stg:{EDWPWD}@EDWHDEV", max_identifier_length=128)
OBCONN = ObieeConnector(login["uid"], login["pwd"])

def snakecase(x) -> str:
    x = re.sub(r"\s+", "_", x)
    return x.lower()


def create_view(sql_path, engine=ENGINE) -> None:
    with open(sql_path) as f:
        query = f.read()

    engine.execute(query)


def get_obiee_report(report_path) -> pd.DataFrame:
    df = OBCONN.get_csv(report_path)
    logger.info(f"pulling OBIEE report: {report_path.split('/')[-1]} - OK")

    df.columns = [snakecase(x) for x in df.columns]
    return df


def get_db_query(query, engine=ENGINE) -> pd.DataFrame:
    return pd.read_sql(query, engine)


def get_report_months(start, end=None):
    """start, end: '%Y%m' """
    start_mon = datetime.strptime(start, "%Y%m")
    months = []

    curr_mon = datetime.today().replace(day=1)

    while curr_mon >= start_mon:
        months.append(curr_mon)
        curr_mon -= relativedelta(months=1)

    return [int(x.strftime("%Y%m")) for x in months]


def months_back(x):
    """returns month_id of x months prior"""
    today = datetime.today()
    month = today - relativedelta(months=x)
    return month.strftime("%Y%m")


def rclone_copy(copy=SHAREPOINT_COPY):
    if SHAREPOINT_COPY:
        if ENV == 'PROD':
            path = 'MBEP'
        elif ENV == "DEV":
            path = 'MBEP_DEV'
            if FEATURE: 
                path = os.path.join(path, FEATURE)

        cmd = "rclone copy -v ../data sharepoint_os:" + path
        os.system(cmd)
