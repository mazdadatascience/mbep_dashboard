#!/usr/bin/env python
import os

from calculate_digital_service_metrics import (
    calc_measures_digital_service, calc_measures_digital_service_extra)
from config import REPORT_MONTHS, SHAREPOINT_COPY
from cv_calculation import get_fact_cv_reg
from dealer_enrollment import get_dealer_enrollment
from digital_service_update_history import get_fact_digital_service_updt
from dlr_export import get_dim_dealers
from export_cx360 import get_cx360_response
from filter_mbep_ro import get_fact_mbep_ro
from generate_dim_date import get_dim_dates
from module_logger import logger
from util import months_back, rclone_copy


def main():
    os.environ["NLS_LANG"] = ".AL32UTF8"

    try:
        get_dim_dealers()
        get_dim_dates()

        # CV
        get_fact_cv_reg()

        # Digital Service
        df_enroll = get_dealer_enrollment()
        start = months_back(REPORT_MONTHS)
        df_ro = get_fact_mbep_ro(start_month=start, df_enrollment=df_enroll)
        df_cx360 = get_cx360_response(start_month=start)
        calc_measures_digital_service(df_ro)
        calc_measures_digital_service_extra(df_ro, df_cx360)
        get_fact_digital_service_updt()

        if SHAREPOINT_COPY:
            logger.info("All files refreshed")
            rclone_copy()
            logger.info("Sharepoint copied")

    except Exception as e:
        logger.exception(e)
        logger.warning("File refresh failed")


if __name__ == "__main__":
    main()
