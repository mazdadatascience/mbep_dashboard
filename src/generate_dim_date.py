from datetime import datetime

import pandas as pd


def get_dim_dates():
    today = datetime.today().strftime("%Y%m%d")

    df = pd.DataFrame({"date": pd.date_range("20000101", today)})
    df["id"] = df["date"].dt.strftime("%Y%m%d")
    df["year"] = df["date"].dt.year
    df["month"] = df["date"].dt.month
    df["day"] = df["date"].dt.day
    df["yearmon"] = df["date"].astype("datetime64[M]")
    df["yearmon_text"] = df["date"].dt.strftime("%b %Y")
    df["month_id"] = df["year"] * 100 + df["month"]
    df = df.set_index("id")

    df.to_csv("../data/dim_date.csv")

    return df

