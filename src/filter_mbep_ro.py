#!/usr/bin/env python
"""
combines exported RO from OBIEE with daily staged digital service response data
EDW_STG.

Filtering from RO header:
1) CP and warranty only 
2) RO completion date must be 60 days or more from RDR date
3) Only enrolled dealers for the month is considered
"""
import os
import re
from datetime import datetime
from time import time
from typing import Dict, List

import mazdap as mz
import pandas as pd
from dateutil.relativedelta import relativedelta
from sqlalchemy import types as sqltypes

from config import PATH_SUFFIX
from export_ro import export_obiee_ro_to_db
from module_logger import logger
from util import ENGINE, create_view, get_report_months


def get_fact_mbep_ro(start_month: str, df_enrollment: pd.DataFrame):
    """
    Gathers MBEP relevant ROs
    1. Export RO headers from OBIEE
    Filters:
        * RO complete - RDR day >= 60
        * CP/Warr only (calculates from line amounts)
    2. Summarize digital service RO responses
    3. Join filtered ROs to digital service RO responses 
    4. left join exported ROs with enrolled dealers


    """
    export_obiee_ro_to_db(start_month)
    summarize_digital_service_response()
    df_ro = get_joined_ro_response(start_month)

    # left join with enrolled dealers
    report_months = get_report_months(start=start_month)
    df_enrolled_dlrs = get_mbep_enrolled_dealers(df_enrollment, report_months)
    df_ro_mbep = df_ro.merge(df_enrolled_dlrs, how="left", on=["dlr_cd", "month_id"])
    df_ro_mbep["enrolled"] = df_ro_mbep["enrolled"].fillna(0)
    df_ro_mbep["mbep_state"] = df_ro_mbep["state"].map(
        lambda x: 0 if x in ["KY", "NC"] else 1
    )
    df_ro_mbep["updt_dt"] = datetime.today()

    logger.debug("RO counts for enrolled dealers")
    logger.debug(df_ro_mbep.groupby(["month_id", "enrolled"]).size())

    # Export
    sql_dtype = {
        "dlr_cd": sqltypes.VARCHAR(5),
        "ro_number": sqltypes.VARCHAR(30),
        "vin": sqltypes.VARCHAR(20),
        "month_id": sqltypes.VARCHAR(6),
        "rpr_complete_dt": sqltypes.VARCHAR(8),
        "ro_sale_dt_diff": sqltypes.NUMERIC,
        "pay_type": sqltypes.VARCHAR(1),
        "cust_total_amt": sqltypes.FLOAT,
        "cust_part_amt": sqltypes.FLOAT,
        "time_sent": sqltypes.TIMESTAMP(),
        "time_viewed": sqltypes.TIMESTAMP(),
        "vid_viewed": sqltypes.FLOAT,
        "vendor_name": sqltypes.VARCHAR(60),
        "enrolled": sqltypes.INTEGER,
        "state": sqltypes.VARCHAR(2),
        "mbep_state": sqltypes.INTEGER,
        "updt_dt": sqltypes.DATE,
    }

    try:
        if PATH_SUFFIX:
            table = "_".join(["fw_ds_mbep_ro", PATH_SUFFIX]).lower()
        else:
            table = "fw_ds_mbep_ro"

        df_ro_mbep.to_sql(
            table, ENGINE, if_exists="replace", index=False, dtype=sql_dtype
        )
        df_ro_mbep.to_csv("../data/fact_mbep_ro.csv", index=False)
        logger.info(f"Staging: MBEP ROs to {table}")
    except Exception as e:
        logger.error(e)

    return df_ro_mbep


def summarize_digital_service_response() -> None:
    """Creating RO summary view per RO number/VIN"""
    path = "../sql/view_fw_ro_response_summary.sql"
    logger.debug("Creating digital service RO response summary view")
    create_view(path)


def get_joined_ro_response(start_month, engine=ENGINE) -> pd.DataFrame:
    """Joining Mazda RO's with summarized RO response"""
    with open("../sql/ro_response.sql") as f:
        query = f.read()

    if PATH_SUFFIX:
        tbl = "_".join(["fw_ro_export", PATH_SUFFIX]).lower()
    else:
        tbl = "fw_ro_export"

    query = query.format(ro_export_tbl=tbl)

    df = pd.read_sql(query, engine, params={"startmo": start_month + "01"})
    logger.debug("Joined Mazda ROs with RO response")
    return df


def get_mbep_enrolled_dealers(df_enrollment, months: List[str]) -> pd.DataFrame:
    """Calculate MBEP enrollment status for list of months
    * mbep_start_mon is the first month of qualification
    * mbep_end_mon is the last month of qualification
    * Lines are not considered unless addendum signed

    ---
    df_enrollment: enrollment mstr table
    months: list of YYYYMM
    """
    df_signed = df_enrollment.query("~addendum_signed_date.isna()").copy()
    dfs = []
    for month in months:
        month = str(month)
        assert len(month) == 6
        started = df_signed["mbep_start_mon"] <= month  # includes mbep_start_mon
        ended = (
            df_signed["mbep_end_mon"] < month
        )  # ended shouldn't include mbep_end_mon
        df_signed["enrolled"] = started & (~ended)

        # Combine the enrollment status in cases of multiple vendors
        # Dealer is enrolled if the boolean sum > 0 (enrolled with one or more vendor for the month)
        dlr = df_signed.groupby("dlr_cd")["enrolled"].sum()
        enrolled_dlr = dlr[dlr > 0].index.to_list()

        df = pd.DataFrame({"dlr_cd": enrolled_dlr, "month_id": month})
        df["enrolled"] = 1
        dfs.append(df)

    df_dealer_month = pd.concat(dfs, axis=0, ignore_index=True)
    return df_dealer_month
