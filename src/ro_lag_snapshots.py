#!/usr/bin/env python

import pandas as pd
from sqlalchemy import types as sqltypes
from module_logger import logger
from util import ENGINE

def main() -> None:
    query = """
    SELECT dlr_cd,
           MAX(RPR_COMPLETE_DT) RPR_COMPLETE_DT,
           MAX(updt_dt) report_dt,
           TRUNC(MAX(updt_dt),'DDD') -MAX(RPR_COMPLETE_DT) - 1 lag_days
    FROM FW_RO_EXPORT fre
    GROUP BY DLR_CD
    ORDER BY  lag_days
    """
    df = pd.read_sql(query, ENGINE)

    cols = {
        "dlr_cd":sqltypes.VARCHAR(30),
        "rpr_complete_dt":sqltypes.DATE,
        "report_dt":sqltypes.DATE,
        "lag_days":sqltypes.FLOAT,
    }
    df.to_sql("fw_ds_ro_lag_snapshot", ENGINE, index=False, dtype=cols, if_exists="append")

if __name__ == '__main__':
    main()
