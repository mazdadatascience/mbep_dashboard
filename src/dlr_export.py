#!/usr/bin/env python
from util import get_obiee_report


def get_dim_dealers():
    p = "/shared/Operational Strategy/Governance and Process Innovation/Data Exports/All Dealers"
    df = get_obiee_report(p)
    df = df[df["dealer_code"].str.contains(r"[\d]{5}", regex=True, na=False)]
    df = df[df.region_code.isin(["GU", "NE", "PA", "MW"])]
    df = df.query("status_code == 'A' & country_code == 'US' & dealer_code")
    df.to_csv("../data/dim_dealers.csv", index=False)

    return df