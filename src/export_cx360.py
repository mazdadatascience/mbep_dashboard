import pandas as pd

from config import PATH_SUFFIX
from module_logger import logger
from util import OBCONN


def get_cx360_response(start_month) -> pd.DataFrame:
    """
    get CX360 survey results from OBIEE

    Args:
        start_month (str): "YYYYMM"

    Returns:
        pd.DataFrame
    """

    query = """
    SELECT "CX - CX360 Survey Detail"."Dealer"."Dealer Code" s_1,
       "CX - CX360 Survey Detail"."Fact - Customer Survey"."Repair Order ID" s_2,
       "CX - CX360 Survey Detail"."Fact - Customer Survey"."Response" s_3,
       "CX - CX360 Survey Detail"."Repair Complete Date"."Calendar Year Month Id" s_4,
       "CX - CX360 Survey Detail"."Survey"."Questions" s_5,
       "CX - CX360 Survey Detail"."Survey"."Survey Type" s_6,
       "CX - CX360 Survey Detail"."Vehicle Details"."VIN Code" s_7
    FROM "CX - CX360 Survey Detail"
    WHERE (
        ("Response Date"."Calendar Date" >= DATE '2021-01-01') 
        AND ("Survey"."Survey Type" = 'Service') 
        AND ("Survey"."Questions" IN ('Service Quality OSAT', 'Service Likelihood to Return', 'Service Overall Satisfaction'))
        AND ("Repair Complete Date"."Calendar Year Month Id" >= {start_month})
    )
    """

    cols = {
        "Dealer Code": "dlr_cd",
        "VIN Code": "vin",
        "Repair Order ID": "ro_number",
        "Questions": "question",
        "Response": "response",
    }


    logger.info("Getting CX360 response from OBIEE")
    df = OBCONN.exec_sql(query.format(start_month=start_month))
    df = df[cols.keys()].rename(columns=cols)
    df['ro_number'] = df['ro_number'].str.lstrip('0')
    df['question'] = df['question'].map(fmt_question)
    df['response'] = df['response'].astype(float)

    wide = df.groupby(['dlr_cd', 'vin', 'ro_number', 'question']).mean().unstack()
    wide.columns = wide.columns.droplevel(0)
    wide.columns.name = None
    wide = wide.reset_index()
    wide

    return wide


def fmt_question(question:str) -> str:
    """
    to lower case and replace spaces with underscores

    Args:
        question (str): survey question string

    Returns:
        str: formatted question string
    """
    return question.lower().replace(" ", '_')


