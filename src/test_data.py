#!/usr/bin/env python
#%%
from datetime import datetime as dt
from dateutil.relativedelta import relativedelta 
import pandas as pd

from dealer_enrollment import get_dealer_enrollment
from filter_mbep_ro import  get_mbep_enrolled_dealers, get_joined_ro_response
from calculate_digital_service_metrics import calc_measures_digital_service 
from util import get_db_query, get_report_months

MONTHS_AGO=3

df_enroll = get_dealer_enrollment()
report_months = get_report_months(start='202109')
enroll_flags = get_mbep_enrolled_dealers(df_enroll, report_months)

#%%
def get_mbep_ro(months_back=MONTHS_AGO):
    start_month = dt.today() - relativedelta(months=months_back)
    month_id = start_month.strftime('%Y%m')

    mbep_ro = get_db_query("""
        SELECT  count(distinct ro_number) ro_count
            , sum(VID_VIEWED) view_count
            ,  DLR_CD 
            , MONTH_ID 
            , PAY_TYPE 
            , MBEP_STATE 
        FROM FW_DS_MBEP_RO fdmr 
        WHERE MONTH_ID >= '{month_id}'
        GROUP BY dlr_cd, month_id, PAY_TYPE, MBEP_STATE 
    """.format(month_id=month_id))
    return mbep_ro

def get_updt_dt():
    q = """
    SELECT MAX(RPR_COMPLETE_DT) max_ro_complete_dt,
       MAX(updt_dt) extract_dt,
       dlr_cd
    FROM FW_RO_EXPORT fre
    GROUP BY dlr_cd
    """

    return get_db_query(q)

mbep_ro = get_mbep_ro()

#%%
mbep_metrics = (
    mbep_ro
    .melt(id_vars=['dlr_cd', 'month_id', 'pay_type', 'mbep_state'])
    .assign(variable = lambda df: df.variable + '_' + df.pay_type)
    .pivot(index=['dlr_cd', 'month_id', 'mbep_state'], columns='variable', values='value')
    .reset_index()
    .rename_axis(None, axis=1)
)

updt_dt = get_updt_dt()

#%%
report = (   
    enroll_flags
    .merge(mbep_metrics, on =["dlr_cd", "month_id"], how='outer')
    .sort_values([ 'dlr_cd' , 'month_id' ], ascending=[True,False])
    .assign(enrolled = lambda df: [1 if x == 1 else 0 for x in df.enrolled])
    .fillna({"mbep_state":0})
    .merge(updt_dt, on =["dlr_cd"], how='left')
)


today = dt.today().strftime('%Y%m%d_%H%M') 
filename = '../test_data/{}_MBEP_check(Sep-Dec_MTD).xlsx'.format(today)
report.to_excel(filename, index=False)

