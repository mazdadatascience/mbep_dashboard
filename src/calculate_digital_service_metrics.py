#!/usr/bin/env python
import re
from datetime import datetime as dt

import numpy as np
import pandas as pd
from sqlalchemy import types as sqltypes

from config import PATH_SUFFIX
from module_logger import logger
from util import ENGINE


def calc_measures_digital_service(df_mbep_ro) -> None:
    """
    * Calculated monthly and R3 metrics for  digital service (all dealer).
    * Percentile rank calculation is done only for MBEP enrolled dealers.

    Args:
        df_mbep_ro (pd.DataFrame): MBEP relevant ROs
        df_cx360 (pd.DataFrame): CX 360 survey responses
    """
    logger.debug("Calculating digital service measures")
    df_counts = calculate_count_rates(df_mbep_ro)
    df_measures = calculate_MBEP_qualification(df_counts, df_mbep_ro)

    df_measures.to_csv("../data/measures_digital_service.csv", index=False)
    stage_metrics(df_measures, "fw_ds_metrics")
    return None


def calculate_count_rates(df: pd.DataFrame) -> pd.DataFrame:
    """
    Monthly and R3 counts and rates

    Args:
        df (pd.DataFrame): MBEP relevant ROs and survey results
    """
    df = df.copy()
    df["vid_sent"] = flag_not_na(df["time_sent"])
    df["ro_count"] = 1
    df["cp_count"] = [1 if x == "C" else 0 for x in df["pay_type"]]

    # Need to count servey cols because not all survey questions are answered
    survey_cols = [x for x in df.columns if re.match("service_", x)]
    for col in survey_cols:
        df["count_" + col] = flag_not_na(df[col])

    # Metric columns
    metrics = [
        "ro_count",
        "cp_count",
        "vid_sent",
        "vid_viewed",
    ]

    # Making monthly and R3 groups
    grp = ["dlr_cd", "month_id"]
    metrics_monthly = df.groupby(grp)[metrics].sum()
    metrics_r3 = metrics_monthly.groupby(grp[:-1]).rolling(3).sum()

    def __calc_rate(df) -> pd.DataFrame:
        df = df.copy()
        df["vid_sent_rate"] = df["vid_sent"] / df["ro_count"]
        df["vid_view_rate"] = df["vid_viewed"] / df["ro_count"]
        return df

    monthly = __calc_rate(metrics_monthly).reset_index()
    r3 = (
        __calc_rate(metrics_r3)
        .reset_index(drop=True)
        .rename(columns=lambda x: x + "_r3")
    )

    df = pd.concat([monthly, r3], axis=1)
    return df


def calculate_MBEP_qualification(
    df_metrics: pd.DataFrame,
    df_mbep: pd.DataFrame,
    percentile_cutoff: int = 25,
    view_rate_cutoff: int = 30,
) -> pd.DataFrame:
    """
    Calculates MBEP qualification
    1) percentile rank only done for MBEP enrolled and MBEP state dealers.
    2) Percentile is rounded to two decimal places before flagging for cutoff

    Args:
        df_metrics (pd.DataFrame): metrics DFs
        df_mbep (pd.DataFrame): MBEP DF for the enrollment status
        percentile_cutoff (int, optional): percentile rank cutoff for MBEP qualification. Defaults to 25.
        view_rate_cutoff (int, optional): view rate cutoff for MBEP qualification. Defaults to 30.

    Returns:
        pd.DataFrame: metrics DF with percentile ranks
    """
    status = (
        df_mbep[["dlr_cd", "month_id", "enrolled", "mbep_state"]]
        .drop_duplicates()
        .reset_index(drop=True)
    )

    try:
        assert df_metrics.shape[0] == status.shape[0]
    except:
        logger.warning(
            "Doesn't have enough enrollement status for percentile calculation"
        )

    df = df_metrics.merge(status, on=["dlr_cd", "month_id"])

    # 1. Rank all dealers/month
    df["vid_view_rank_r3"] = df.groupby(["month_id", "enrolled", "mbep_state"])[
        "vid_view_rate_r3"
    ].rank(method="max")

    # 1b. Exclude non-enrolled and non-mbep states
    df.loc[df["enrolled"] != 1, "vid_view_rank_r3"] = np.NaN
    df.loc[df["mbep_state"] != 1, "vid_view_rank_r3"] = np.NaN

    # 1c. Count MBEP dealers for each month
    mbep_dealer_count = (
        df.query("enrolled == 1 & mbep_state == 1")
        .groupby(["month_id"])
        .apply(lambda df: df["vid_view_rank_r3"].notna().sum())
        .reset_index(name="mbep_dlr_count")
    )
    df = df.merge(mbep_dealer_count, on="month_id", how="left")

    # 1d. Percentile calculation and rounding
    df["vid_view_percentile_r3"] = df["vid_view_rank_r3"] / df["mbep_dlr_count"]
    df["vid_view_percentile_r3"] = df["vid_view_percentile_r3"].round(2)

    # 2. calculate mbep qualification flag
    con1 = df["vid_view_percentile_r3"] >= percentile_cutoff / 100
    con2 = df["vid_view_rate_r3"] >= view_rate_cutoff / 100
    df["mbep_qualified"] = [1 if x else 0 for x in (con1 & con2)]

    df = df.set_index(
        ["dlr_cd", "month_id", "enrolled", "mbep_state", "mbep_qualified"]
    ).reset_index()

    return df


def stage_metrics(df: pd.DataFrame, table="fw_ds_metrics"):
    """
    writing to sandbox, adding update time and sqltypes
    """
    if PATH_SUFFIX:
        table_nm = "_".join([table, PATH_SUFFIX]).lower()
    else:
        table_nm = table

    df["updt_dt"] = dt.today()
    dtype = {col: sqltypes.FLOAT for col in df.columns}
    dtype.update(
        {
            "month_id": sqltypes.VARCHAR(6),
            "dlr_cd": sqltypes.VARCHAR(5),
            "updt_dt": sqltypes.DATE,
        }
    )

    logger.info(f"Staging: digital service metrics to {table_nm}")
    df.to_sql(table_nm, ENGINE, dtype=dtype, index=False, if_exists="replace")


def flag_not_na(x: pd.Series) -> int:
    """
    NaN -> 0; other -> 1
    """
    return [0 if x else 1 for x in x.isna()]


def calc_measures_digital_service_extra(df_mbep_ro, df_cx360) -> None:
    """
    * Extra metrics for digital service that for comparision between viewed and non-viewed RO

    Args:
        df_mbep_ro (pd.DataFrame): MBEP relevant ROs
        df_cx360 (pd.DataFrame): CX 360 survey responses
    """
    logger.debug("Calculating extra digital service measures")
    df = df_mbep_ro.merge(df_cx360, on=["dlr_cd", "ro_number", "vin"], how="left")

    # ro columns
    ro_cols = [
        "ro_count",
        "cp_count",
    ]
    df["ro_count"] = 1
    df["cp_count"] = [1 if x == "C" else 0 for x in df["pay_type"]]

    # Value columns
    service_cols = [x for x in df.columns if re.match("service_", x)]
    value_cols = [
        "cust_total_amt",
        "cust_part_amt",
    ]
    value_cols.extend(service_cols)

    # Create counter columns
    for col in value_cols:
        df[col] = df[col].astype(float)
        df["count_" + col] = flag_not_na(df[col])
    count_cols = ["count_" + x for x in value_cols]

    # Making monthly and R3 groups
    grp = ["vid_viewed", "dlr_cd", "month_id"]
    metrics_monthly = df.groupby(grp)[[*ro_cols, *value_cols, *count_cols]].sum()
    metrics_r3 = metrics_monthly.groupby(grp[:-1]).rolling(3).sum()

    def __calc_rate(df_ref) -> pd.DataFrame:
        df = df_ref.copy()
        for col in value_cols:
            df[col] = df[col] / df["count_" + col]
        return df

    monthly = __calc_rate(metrics_monthly).reset_index()
    r3 = (
        __calc_rate(metrics_r3)
        .reset_index(drop=True)
        .rename(columns=lambda x: x + "_r3")
    )
    df = (
        pd.concat([monthly, r3], axis=1)
        .drop(columns=[x for x in df.columns if x.startswith("count_")])
        .sort_values(
            ["month_id", "dlr_cd", "vid_viewed"], ascending=[False, True, True]
        )
    )

    stage_metrics(df, "fw_ds_metrics_extra")
    df.to_csv("../data/measures_digital_service_extra.csv", index=False)
    return df
