#!/usr/bin/env python
#%%
import pandas as pd

from util import OBCONN

def get_fact_cv_reg():
    """Limit CV registration count to vehicles sold within the R3 cycle
    """
    query = """
    SELECT
        XSA('weblogic'.'MARITZ_CV_VIN_DETAIL')."Columns"."DLR_CD" s_2,
        XSA('weblogic'.'MARITZ_CV_VIN_DETAIL')."Columns"."CV_ACTIVATION_DT" s_1,
        XSA('weblogic'.'MARITZ_CV_VIN_DETAIL')."Columns"."RDR_DATE" s_3,
        XSA('weblogic'.'MARITZ_CV_VIN_DETAIL')."Columns"."VIN" s_4
    FROM XSA('weblogic'.'MARITZ_CV_VIN_DETAIL')
    """

    col = ["dlr_cd", "cv_activation_dt", "rdr_dt", "vin"]
    df = OBCONN.exec_sql(query, custom_cols=col)
    df["cv_activation_dt"] = df["cv_activation_dt"].astype("datetime64[D]")
    df["rdr_dt"] = df["rdr_dt"].astype("datetime64[D]")
    df["cv_month"] = df["cv_activation_dt"].astype("datetime64[M]")
    df["rdr_month"] = df["rdr_dt"].astype("datetime64[M]")

    df["activated_within_r3"] = df["rdr_month"] >= (df["cv_month"] - pd.offsets.MonthBegin(2))
    df['exclude'] = df.cv_month.notna() & (~df.activated_within_r3)

    df["cv_month"] = df["cv_month"].dt.strftime("%Y%m%d")
    df["rdr_month"] = df["rdr_month"].dt.strftime("%Y%m%d")

    df.to_csv("../data/testing_cv_check.csv", index=False)

    df_cv = (
        df.groupby(["cv_month", "dlr_cd", "exclude"])
        .size()
        .reset_index(name="cv_count")
        .query("~exclude")
        .drop(columns=["exclude"])
        .rename(columns={"cv_month": "month"})
    )

    df_rdr = (
        df.groupby(["rdr_month", "dlr_cd"])
        .size()
        .reset_index(name="rdr_count")
        .rename(columns={"rdr_month": "month"})
    )

    df = df_cv.merge(df_rdr, on=["dlr_cd", "month"])
    df = df.sort_values(["dlr_cd", "month"]).reset_index(drop=True)
    df_r3 = (
        df.groupby("dlr_cd")
        .rolling(3)
        .agg({"cv_count": sum, "rdr_count": sum})
        .reset_index(drop=True)
    )
    df_r3.columns = [x + "_r3" for x in df_r3.columns]

    df_out = pd.concat([df, df_r3], axis=1)
    df_out.to_csv("../data/fact_cv_reg.csv", index=False)
    return df_out


# %%
get_fact_cv_reg()
