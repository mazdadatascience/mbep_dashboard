SELECT DISTINCT t1.dealer_code,
       REGEXP_REPLACE(vendor_name,'[^A-Za-z0-9/]','') vendor_name,
       t1.LAST_UPDT_DT
FROM (SELECT dealer_code,
             MAX(last_updt_dt) LAST_UPDT_DT
      FROM edw_stg.digital_service_ro_response
      WHERE vendor_name IS NOT NULL
      AND   DEALER_CODE IS NOT NULL
      GROUP BY dealer_code
      ORDER BY DEALER_CODE) t1
  LEFT JOIN edw_stg.digital_service_ro_response ro
         ON t1.last_updt_dt = ro.LAST_UPDT_DT
        AND t1.dealer_code = ro.DEALER_CODE
ORDER BY DEALER_CODE