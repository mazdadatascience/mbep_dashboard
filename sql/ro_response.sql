-- Joining all Mazda RO's from valid dealers with digital service reponse
SELECT dlr_cd,
    ro.vin vin,
    ro_claim_cd ro_number,
    TO_CHAR(rpr_complete_dt,'YYYYMM') month_id,
    TO_CHAR(rpr_complete_dt,'YYYYMMDD') rpr_complete_dt,
    ro_sale_dt_diff,
    pay_type,
    cust_total_amt,
    cust_part_amt,
    time_sent,
    time_viewed,
    vid_viewed,
    vendor_name,
    state
FROM {ro_export_tbl} ro
LEFT JOIN view_fw_ro_response ds
        ON LTRIM (ro.RO_CLAIM_CD,'0') = ds.ro_number
        AND ro.vin = ds.vin
WHERE ro_sale_dt_diff >= 60
AND rpr_complete_dt >= to_date(&startmo, 'YYYYMMDD')
AND   PAY_TYPE IN ('C','W')