-- View to determine dealer MBEP qualification period
-- Enrollment rules:
--    1. Eligible for the entire month even if enrolled on the last day 
--    2. Eligible for the entire month if service ended on the very last day on the month 
--          2a. Service ended on 7/31 -> eligible up to  July
--          2b. Service ended on 7/30 -> eligible up to June


CREATE OR REPLACE VIEW VIEW_FW_DS_DEALER_ENROLLMENT AS
SELECT RANK() OVER (PARTITION BY dlr_code ORDER BY ENROLLMENT_START_DATE DESC) vendor_rank,
       TO_CHAR(dlr_code) dlr_cd,
       enrollment_start_date,
       service_end_date,
       notice_to_cancel_date,
       launch_day,
       addendum_signed_date,
       SUBSTR(mbep_addendum_signed,1,1) mbep_addendum_signed,
       TO_CHAR(enrollment_start_date,'YYYYMM') mbep_start_mon,
       TO_CHAR(ADD_MONTHS(service_end_date +1,-1),'YYYYMM') mbep_end_mon,
       vendor_name
FROM edw_stg.DIG_SERV_DLR_ENROLMENT_MSTR