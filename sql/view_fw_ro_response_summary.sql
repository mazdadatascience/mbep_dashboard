-- Summarized RO append
-- * ro_number IS trimmed
-- * use latest DATE FOR ro_date
-- * use latest time FOR time sent
-- * use latest time FOR time viewed
-- * ADD up ALL VIEW counts
-- * Remove non alpha CHARACTERS FROM vendor name
CREATE OR REPLACE VIEW view_fw_ro_response 
AS
SELECT TRIM(vin) VIN,
       LTRIM(RO_NUMBER,'0') AS RO_NUMBER,
       MAX(RO_DATE) AS ro_date,
       MAX(TIME_SENT) AS TIME_SENT,
       MAX(TIME_VIEWED) AS TIME_VIEWED,
       SUM(VIEW_COUNT) AS VIEW_COUNT,
       REGEXP_REPLACE(vendor_name,'[^A-Za-z0-9/]','') AS VENDOR_NAME,
       CASE
         WHEN SUM(NVL(VIEW_COUNT,0)) > 0 THEN 1
         ELSE 0
       END AS VID_VIEWED,
       1 AS VID_SENT
FROM EDW_STG.DIGITAL_SERVICE_RO_RESPONSE
WHERE (NVL(TRIM(VIDEO_LENGTH),'0:00:00') NOT IN ('0:00:00','00:00:00','0'))
GROUP BY TRIM(vin),
         REGEXP_REPLACE(vendor_name,'[^A-Za-z0-9/]',''),
         LTRIM(RO_NUMBER,'0')