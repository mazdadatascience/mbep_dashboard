# Tally CV and RDR counts
import pandas as pd


def tally_by_month(df, col, outname):
    month_col = col + "_mo"
    df[month_col] = df[col].astype("datetime64[M]")
    out = df.groupby(["dlr_cd", month_col]).size().reset_index()
    out.columns = ["dlr_cd", "month", outname]

    return out


# Process
df_cv = pd.read_csv("./data/CV_data.csv")
df_cv.columns = [x.lower() for x in df_cv.columns]
rdr = tally_by_month(df_cv, "rdr_date", "rdr_count")
cv = tally_by_month(df_cv, "cv_activation_dt", "cv_activation_count")
df_out = pd.merge(rdr, cv, on=["dlr_cd", "month"], how="outer")

# output
df_out.to_csv("./data/cv_rdr_counts.csv", index=False)

